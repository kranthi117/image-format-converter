var async = require("async");
var AWS = require("aws-sdk");

var im = require("gm").subClass({imageMagick: true});
var s3 = new AWS.S3();
var fs = require("fs")

var CONFIG = require("./config.json");

function getImageType(objectContentType) {
	if (objectContentType === "image/jpeg") {
		return "jpeg";
	} else if (objectContentType === "image/png") {
		return "png";
	} else {
		throw new Error("unsupported objectContentType " + objectContentType);
	}
}

exports.handler = function(event, context) {
	console.log("event ", JSON.stringify(event));
	async.mapLimit(event.Records, CONFIG.concurrency, function(record, cb) {
		var originalKey = decodeURIComponent(record.s3.object.key.replace(/\+/g, " "));
		s3.getObject({
			"Bucket": record.s3.bucket.name,
			"Key": originalKey
		}, function(err, data) {
			if (err) {
				console.log("Unable to download the image from s3")
				cb(err);
			} else {
				cb(null, {
					"originalKey": originalKey,
					"contentType": data.ContentType,
					"imageType": getImageType(data.ContentType),
					"buffer": data.Body,
					"record": record
				});
			}
		});
	}, function(err, images) {
		if (err) {
			context.fail(err);
		} else {
			async.eachLimit(images, CONFIG.concurrency, function(image, cb) {
				im(image.buffer).write('/tmp/out.webp', function (err) {
					if (err) {
						console.log("Unable to do imagemagick");
						cb(err);
					} else {
						fs.readFile('/tmp/out.webp', function (err, data) {
							if (err) {
								console.log('Unable to read the temp file');
								cb(err);
							} else {
								s3.putObject({
									"Bucket": image.record.s3.bucket.name + "-webp",
									"Key": image.originalKey.replace('.' + image.imageType, '.webp'),
									"Body": data,
									"ContentType": 'image/webp',
									"ACL": 'public-read'
								}, function(err) {
									console.log("Unable to upload to s3");
									cb(err);
								});
							}
						});
					}
				});
			}, function(err) {
				if (err) {
					console.log("async failed")
					context.fail(err);
				} else {
					console.log("async succeed")
					context.succeed();
				}
			});
		}
	});
};
